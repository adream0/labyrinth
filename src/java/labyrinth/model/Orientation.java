package labyrinth.model;

/**
 * @author jasim
 */
public enum Orientation {
    HORIZONTAL,
    VERTICAL
}
