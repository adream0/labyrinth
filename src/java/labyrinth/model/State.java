package labyrinth.model;

/**
 * @author jasim
 */
public class State {
    private GraphNode node;
    private Orientation orientation;
    private int distance;

    public State(GraphNode node, Orientation orientation, int distance) {
        this.node = node;
        this.orientation = orientation;
        this.distance = distance;
    }

    public GraphNode getNode() {
        return node;
    }

    public Orientation getOrientation() {
        return orientation;
    }
    public int getDistance() {
        return distance;
    }

}
