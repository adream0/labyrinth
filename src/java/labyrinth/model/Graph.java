package labyrinth.model;

/**
 * @author jasim
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {
    private Map<Integer, GraphNode> nodes;
    private GraphNode startNode;
    private GraphNode endNode;
    private int numRows;
    private int numCols;
    private boolean hasDimensions;

    public Graph() {
        nodes = new HashMap<>();
    }

    public void addNode(int id, GraphNode node) {
        nodes.put(id, node);
        if (node.isStartNode()) {
            startNode = node;
        } else if (node.isEndNode()) {
            endNode = node;
        }
    }


    public GraphNode getNode(int row, int col) {
        for (GraphNode node : nodes.values()) {
            if (node.getRow() == row && node.getCol() == col) {
                return node;
            }
        }
        // Si no se encuentra el nodo en las coordenadas dadas
        return null;
    }


    public List<GraphNode> getAllNodes() {
        return new ArrayList<>(nodes.values());
    }

    public GraphNode getStartNode() {
        return startNode;
    }

    public void setStartNode(GraphNode startNode) {
        this.startNode = startNode;
    }

    public GraphNode getEndNode() {
        return endNode;
    }

    public void setEndNode(GraphNode endNode) {
        this.endNode = endNode;
    }

    public int getNumRows() {
        if (!hasDimensions) {
            calculateDimensions();
        }
        return numRows;
    }

    public int getNumCols() {
        if (!hasDimensions) {
            calculateDimensions();
        }
        return numCols;
    }

    private void calculateDimensions() {
        numRows = nodes.values().stream().mapToInt(GraphNode::getRow).max().orElse(0) + 1;
        numCols = nodes.values().stream().mapToInt(GraphNode::getCol).max().orElse(0) + 1;
        hasDimensions = true;
    }
}
