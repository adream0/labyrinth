package labyrinth.model;

import java.util.Objects;

/**
 * @author jasim
 */
public class Rectangulo {
    private GraphNode startNode;
    private GraphNode middleNode;
    private GraphNode endNode;
    private Orientation orientation;
    private int distance;

    public Rectangulo(GraphNode startNode, GraphNode middleNode, GraphNode endNode, Orientation orientation, int distance ) {
        this.startNode = startNode;
        this.middleNode = middleNode;
        this.endNode = endNode;
        this.orientation = orientation;
        this.distance=distance;
    }

    public GraphNode getStartNode() {
        return startNode;
    }

    public GraphNode getMiddleNode() {
        return middleNode;
    }

    public GraphNode getEndNode() {
        return endNode;
    }

    public Orientation getOrientation() {
        return orientation;
    }
    public int getDistance() {
        return distance;
    }
    @Override
    public int hashCode() {
        return Objects.hash(startNode, middleNode, endNode, orientation);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Rectangulo other = (Rectangulo) obj;
        return Objects.equals(startNode, other.startNode) &&
                Objects.equals(middleNode, other.middleNode) &&
                Objects.equals(endNode, other.endNode) &&
                orientation == other.orientation;
    }
}
