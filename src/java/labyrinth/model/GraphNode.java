package labyrinth.model;

/**
 * @author jasim
 */
public class GraphNode {

    private char data;
    private int row;
    private int col;

    public GraphNode(char data, int row, int col) {
        this.data = data;
        this.row = row;
        this.col = col;
    }

    public char getData() {
        return data;
    }

    public void setData(char data) {
        this.data = data;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public boolean isStartNode() {
        return data == 'S';
    }

    public boolean isEndNode() {
        return data == 'E';
    }
}
