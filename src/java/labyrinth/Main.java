package labyrinth;

import labyrinth.model.Graph;
import labyrinth.model.GraphNode;
import labyrinth.services.inputs.MazeReader;
import labyrinth.services.inputs.impl.FileMazeReader;
import labyrinth.services.solvers.MazeSolver;
import labyrinth.services.solvers.impl.MazeSolverImpl;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {
    private static final String SAMPLES_FOLDER = "src/java/labyrinth/sample/";

    private static final String SAMPLES_EXTENSION = ".txt";
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        // Configurar el logger
        LOGGER.setUseParentHandlers(false);
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new SimpleFormatter() {
            @Override
            public synchronized String format(java.util.logging.LogRecord logRecord) {
                return logRecord.getMessage() + System.lineSeparator();
            }
        });
        LOGGER.addHandler(consoleHandler);
        LOGGER.setLevel(Level.INFO);

        Scanner scanner = new Scanner(System.in);

        boolean exit = false;

        while (!exit) {
            LOGGER.info("Seleccione una opción:");
            LOGGER.info("1. sample1");
            LOGGER.info("2. sample2");
            LOGGER.info("3. sample3");
            LOGGER.info("4. sample4");
            LOGGER.info("0. Salir");

            int option = scanner.nextInt();
            scanner.nextLine();

            if (option == 0) {
                LOGGER.info("Saliendo del programa...");
                break;
            }

            String filename;

            switch (option) {
                case 1 -> filename = "sample1";
                case 2 -> filename = "sample2";
                case 3 -> filename = "sample3";
                case 4 -> filename = "sample4";
                default -> {
                    LOGGER.info("Opción inválida. Se utilizará sample1 por defecto.");
                    filename = "sample1";
                }
            }

            String filePath = SAMPLES_FOLDER + filename + SAMPLES_EXTENSION;

            MazeReader mazeReader = new FileMazeReader(filePath);

            // Leer el laberinto utilizando el MazeReader apropiado
            Graph graph = mazeReader.readMaze();

            // Obtener el tamaño de filas y columnas del laberinto
            int numRows = graph.getNumRows();
            int numCols = graph.getNumCols();

            // Obtener la celda de inicio (superior izquierdo) y la celda de fin (inferior derecho)
            GraphNode startCell = graph.getNode(0, 1);
            GraphNode endCell = graph.getNode(numRows - 2, numCols - 1);

            // Marcar la celda de inicio y la celda de fin en el grafo
            graph.setStartNode(startCell);
            graph.setEndNode(endCell);

            // Seleccionar la estrategia de resolución del laberinto
            MazeSolver solver = new MazeSolverImpl();

            // Resolver el laberinto utilizando la estrategia seleccionada
            int shortestPath = solver.findShortestPath(graph);
            if (shortestPath == -1) {
                LOGGER.info("Camino no encontrado");
            } else {
                LOGGER.info(String.format("El camino más corto para resolver el laberinto es: %d", shortestPath));
            }

            LOGGER.info("Presione una tecla para continuar...");
            try {
                System.in.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
