package labyrinth.services.inputs;

import labyrinth.model.Graph;

/**
 * @author jasim
 */
public interface MazeReader {
    Graph readMaze();
}
