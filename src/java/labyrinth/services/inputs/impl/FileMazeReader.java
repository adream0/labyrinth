package labyrinth.services.inputs.impl;

import labyrinth.model.Graph;
import labyrinth.model.GraphNode;
import labyrinth.services.inputs.MazeReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * @author jasim
 */
public class FileMazeReader implements MazeReader {
    private static final Logger LOGGER = Logger.getLogger(FileMazeReader.class.getName());
    private String filePath;

    public FileMazeReader(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public Graph readMaze() {
        try {
            File file = new File(filePath);
            Scanner fileScanner = new Scanner(file);

            List<String> rows = new ArrayList<>();
            int numRows = 0;
            int numCols = 0;

            // Leer el archivo y obtener las filas del laberinto
            while (fileScanner.hasNextLine() && numRows < 1000) {
                String line = fileScanner.nextLine();
                int lineLength = line.length();
                long validCharsCount = line.chars().filter(c -> c == '.' || c == '#').count();

                // Verificar que la línea cumpla con los constraints
                if (validCharsCount > 0 && lineLength >= 3 && lineLength <= 1000) {
                    rows.add(line);
                    numCols = Math.max(numCols, lineLength);
                    numRows++;
                }
            }

            // Crear el grafo con el tamaño adecuado
            Graph graph = new Graph();

            // Crear los nodos del grafo a partir de los datos de cada celda del laberinto
            for (int rowIndex = 0; rowIndex < numRows; rowIndex++) {
                String row = rows.get(rowIndex);
                int rowLength = row.length();
                int colIndex = 0;
                for (int i = 0; i < rowLength; i++) {
                    char cell = row.charAt(i);
                    if (cell == '.' || cell == '#') {
                        GraphNode node = new GraphNode(cell, rowIndex, colIndex);
                        graph.addNode(rowIndex * numCols + colIndex, node);
                        colIndex++;
                    }
                }
            }

            return graph;
        } catch (FileNotFoundException e) {
            LOGGER.info("Error al leer el archivo. Saliendo del programa.");
            System.exit(0);
        }

        return null;
    }

}
