package labyrinth.services.inputs.impl;

import labyrinth.model.Graph;
import labyrinth.model.GraphNode;
import labyrinth.services.inputs.MazeReader;

import java.util.Scanner;

/**
 * @author jasim
 */
public class ManualMazeReader implements MazeReader {
    private Scanner scanner;

    public ManualMazeReader(Scanner scanner) {
        this.scanner = scanner;
    }
    @Override
    public Graph readMaze() {
        System.out.println("Ingrese el laberinto (formato: [[\".\",\".\",\".\",...], [\"#\",\"#\",\"#\",...], ...]):");
        String input = scanner.nextLine();

        // Eliminar los caracteres innecesarios del input
        input = input.replaceAll("\\[|\\]|\"", "");

        // Dividir la cadena en filas separadas
        String[] rows = input.split(",");

        int numRows = 0;
        int numCols = 0;

        // Contar el número de filas y columnas teniendo en cuenta solo los caracteres válidos
        for (String row : rows) {
            int validCharsCount = 0;
            for (char cell : row.toCharArray()) {
                if (cell == '.' || cell == '#') {
                    validCharsCount++;
                }
            }

            // Verificar que la fila contenga al menos un carácter válido
            if (validCharsCount > 0) {
                numRows++;
                numCols = Math.max(numCols, validCharsCount);
            }
        }

        Graph graph = new Graph();

        // Crear los nodos del grafo
        int rowIndex = 0;
        for (String row : rows) {
            int colIndex = 0;
            for (char cell : row.toCharArray()) {
                if (cell == '.' || cell == '#') {
                    GraphNode node = new GraphNode(cell, rowIndex, colIndex);
                    graph.addNode(rowIndex * numCols + colIndex, node);
                    colIndex++;
                }
            }

            // Incrementar el índice de fila solo si la fila contiene al menos un carácter válido
            if (colIndex > 0) {
                rowIndex++;
            }
        }

        return graph;
    }

}
