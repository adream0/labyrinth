package labyrinth.services.solvers.impl;

import labyrinth.model.Graph;
import labyrinth.model.GraphNode;
import labyrinth.model.Orientation;
import labyrinth.model.Rectangulo;
import labyrinth.services.solvers.MazeSolver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/**
 * @author jasim
 */
public class MazeSolverImpl implements MazeSolver {
    private static final char EMPTY_CELL = '.';
    private static final char WALL_CELL = '#';

    private int contador;


    @Override
    public int findShortestPath(Graph graph) {
        // Obtener el nodo de inicio y el nodo objetivo del laberinto
        GraphNode startNode = graph.getStartNode();
        GraphNode endNode = graph.getEndNode();

        // Verificar si el laberinto es válido
        if (!isValidMaze(graph, startNode, endNode)) {
            return -1;
        }

        // Crear una cola para realizar el recorrido BFS
        Queue<Rectangulo> queue = new LinkedList<>();
        Set<Rectangulo> visitedRectangulos = new HashSet<>();

        // Crear el rectángulo inicial y marcarlo como visitado
        Rectangulo initialRectangulo = new Rectangulo(startNode, graph.getNode(0, 1), graph.getNode(0, 2), Orientation.HORIZONTAL, 0);
        visitedRectangulos.add(initialRectangulo);

        // Realizar el recorrido BFS
        queue.offer(initialRectangulo);

        while (!queue.isEmpty()) {
            Rectangulo currentRectangulo = queue.poll();
            GraphNode currentCell = currentRectangulo.getStartNode();

            // Verificar si hemos llegado al nodo objetivo
            if (currentCell == endNode) {
                System.out.println("Iteraciones realizadas : " + contador);
                return currentRectangulo.getDistance();
            }

            // Obtener los posibles movimientos desde el rectángulo actual
            List<Rectangulo> nextRectangulos = getNextRectangulos(currentRectangulo, graph);

            for (Rectangulo nextRectangulo : nextRectangulos) {
                GraphNode nextCell = nextRectangulo.getEndNode();

                if (nextCell.getData() != WALL_CELL && !visitedRectangulos.contains(nextRectangulo)) {
                    // Agregar el nuevo rectángulo a la cola y marcarlo como visitado
                    queue.offer(nextRectangulo);
                    visitedRectangulos.add(nextRectangulo);
                }
            }
            ++contador;
        }
        // No se encontró una ruta válida hasta el nodo objetivo
        System.out.println("Iteraciones realizadas : " + contador);
        return -1;
    }

    private List<Rectangulo> getNextRectangulos(Rectangulo currentRectangulo, Graph graph) {
        List<Rectangulo> nextRectangulos = new ArrayList<>();

        int distance = currentRectangulo.getDistance();

        GraphNode startNode = currentRectangulo.getStartNode();
        GraphNode middleNode = currentRectangulo.getMiddleNode();
        GraphNode endNode = currentRectangulo.getEndNode();
        Orientation currentOrientation = currentRectangulo.getOrientation();

        int row = startNode.getRow();
        int col = startNode.getCol();

        // Movimiento hacia abajo
        if (canMoveDown(startNode, graph, currentOrientation)) {
            int nextRow = row + 1;
            GraphNode nextStartNode = graph.getNode(nextRow, col);
            GraphNode nextMiddleNode = graph.getNode(nextRow, middleNode.getCol());
            GraphNode nextEndNode = graph.getNode(nextRow, endNode.getCol());
            Rectangulo nextRectangulo = new Rectangulo(nextStartNode, nextMiddleNode, nextEndNode, currentOrientation, distance + 1);
            nextRectangulos.add(nextRectangulo);
        }

        // Movimiento hacia arriba
        if (canMoveUp(startNode, graph, currentOrientation)) {
            int nextRow = row - 1;
            GraphNode nextStartNode = graph.getNode(nextRow, col);
            GraphNode nextMiddleNode = graph.getNode(nextRow, middleNode.getCol());
            GraphNode nextEndNode = graph.getNode(nextRow, endNode.getCol());
            Rectangulo nextRectangulo = new Rectangulo(nextStartNode, nextMiddleNode, nextEndNode, currentOrientation, distance + 1);
            nextRectangulos.add(nextRectangulo);
        }

        // Movimiento a la derecha
        if (canMoveRight(startNode, graph, currentOrientation)) {
            int nextCol = col + 1;
            GraphNode nextStartNode = graph.getNode(row, nextCol);
            GraphNode nextMiddleNode = graph.getNode(middleNode.getRow(), nextCol);
            GraphNode nextEndNode = graph.getNode(endNode.getRow(), nextCol);
            Rectangulo nextRectangulo = new Rectangulo(nextStartNode, nextMiddleNode, nextEndNode, currentOrientation, distance + 1);
            nextRectangulos.add(nextRectangulo);
        }

        // Movimiento a la izquierda
        if (canMoveLeft(startNode, graph, currentOrientation)) {
            int prevCol = col - 1;

            GraphNode nextStartNode = graph.getNode(row, prevCol);
            GraphNode nextMiddleNode = graph.getNode(middleNode.getRow(), prevCol);
            GraphNode nextEndNode = graph.getNode(endNode.getRow(), prevCol);
            Rectangulo nextRectangulo = new Rectangulo(nextStartNode, nextMiddleNode, nextEndNode, currentOrientation, distance + 1);
            nextRectangulos.add(nextRectangulo);
        }

        // Cambio de orientación
        if (canChangeOrientation(startNode, graph)) {
            Orientation nextOrientation = (currentOrientation == Orientation.HORIZONTAL) ?
                    Orientation.VERTICAL : Orientation.HORIZONTAL;
            Rectangulo nextRectangulo = new Rectangulo(startNode, middleNode, endNode, nextOrientation, distance + 1);
            nextRectangulos.add(nextRectangulo);
        }

        return nextRectangulos;
    }


    private boolean canMoveDown(GraphNode cell, Graph graph, Orientation orientation) {
        int row = cell.getRow();
        int col = cell.getCol();

        if (orientation == Orientation.VERTICAL) {
            int nextRow = row + 2;
            if (nextRow >= graph.getNumRows()) {
                return false;
            }

            GraphNode nextCell = graph.getNode(nextRow, col);
            if (nextCell == null || nextCell.getData() != EMPTY_CELL) {
                return false;
            }

        } else if (orientation == Orientation.HORIZONTAL) {
            int nextRow = row + 1;
            int leftCol = col - 1;
            int rightCol = col + 1;

            int numRows = graph.getNumRows();
            int numCols = graph.getNumCols();

            if (nextRow >= numRows || leftCol < 0 || rightCol >= numCols) {
                return false;
            }

            GraphNode nextCellDown = graph.getNode(nextRow, col);
            GraphNode leftAdjacentDownCell = graph.getNode(nextRow, leftCol);
            GraphNode rightAdjacentDownCell = graph.getNode(nextRow, rightCol);

            if (nextCellDown == null || leftAdjacentDownCell == null || rightAdjacentDownCell == null ||
                    nextCellDown.getData() != EMPTY_CELL || leftAdjacentDownCell.getData() != EMPTY_CELL ||
                    rightAdjacentDownCell.getData() != EMPTY_CELL) {
                return false;
            }
        }
        return true;
    }

    private boolean canMoveUp(GraphNode cell, Graph graph, Orientation orientation) {
        int row = cell.getRow();
        int col = cell.getCol();

        if (orientation == Orientation.VERTICAL) {
            int prevRow = row - 2;
            if (prevRow < 0) {
                return false;
            }

            GraphNode prevCell = graph.getNode(prevRow, col);
            if (prevCell == null || prevCell.getData() != EMPTY_CELL) {
                return false;
            }
        } else if (orientation == Orientation.HORIZONTAL) {
            int prevRow = row - 1;
            int leftCol = col - 1;
            int rightCol = col + 1;

            int numCols = graph.getNumCols();

            if (prevRow < 0 || leftCol < 0 || rightCol >= numCols) {
                return false;
            }

            GraphNode prevCellUp = graph.getNode(prevRow, col);
            GraphNode leftAdjacentCellUp = graph.getNode(prevRow, leftCol);
            GraphNode rightAdjacentCellUp = graph.getNode(prevRow, rightCol);

            if (prevCellUp == null || leftAdjacentCellUp == null || rightAdjacentCellUp == null ||
                    prevCellUp.getData() != EMPTY_CELL || leftAdjacentCellUp.getData() != EMPTY_CELL ||
                    rightAdjacentCellUp.getData() != EMPTY_CELL) {
                return false;
            }
        }
        return true;
    }

    private boolean canMoveRight(GraphNode cell, Graph graph, Orientation orientation) {
        int row = cell.getRow();
        int col = cell.getCol();
        int nextCol = col + 1;
        int topCell = row + 1;
        int bottomCell = row - 1;

        int numCols = graph.getNumCols();

        if (orientation == Orientation.VERTICAL) {
            if (nextCol >= numCols) {
                return false;
            }

            GraphNode nextCell = graph.getNode(topCell, nextCol);
            GraphNode topAdjacentCell = graph.getNode(row, nextCol);
            GraphNode bottomAdjacentCell = graph.getNode(bottomCell, nextCol);

            if (nextCell == null || topAdjacentCell == null || bottomAdjacentCell == null ||
                    nextCell.getData() != EMPTY_CELL || topAdjacentCell.getData() != EMPTY_CELL ||
                    bottomAdjacentCell.getData() != EMPTY_CELL) {
                return false;
            }
        }

        if (orientation == Orientation.HORIZONTAL) {
            int nextCell = col + 2;

            if (nextCell >= numCols) {
                return false;
            }

            GraphNode nextCellOptional = graph.getNode(row, nextCell);
            if (nextCellOptional == null || nextCellOptional.getData() != EMPTY_CELL) {
                return false;
            }
        }

        return true;
    }

    private boolean canMoveLeft(GraphNode cell, Graph graph, Orientation orientation) {
        int row = cell.getRow();
        int col = cell.getCol();
        int nextCol = col - 1;
        int topCell = row + 1;
        int bottomCell = row - 1;

        if (orientation == Orientation.VERTICAL) {
            if (nextCol < 0) {
                return false;
            }

            GraphNode nextCell = graph.getNode(topCell, nextCol);
            GraphNode topAdjacentCell = graph.getNode(row, nextCol);
            GraphNode bottomAdjacentCell = graph.getNode(bottomCell, nextCol);

            if (nextCell == null || topAdjacentCell == null || bottomAdjacentCell == null ||
                    nextCell.getData() != EMPTY_CELL || topAdjacentCell.getData() != EMPTY_CELL ||
                    bottomAdjacentCell.getData() != EMPTY_CELL) {
                return false;
            }
        }

        if (orientation == Orientation.HORIZONTAL) {
            int nextCell = col - 2;

            if (nextCell < 0) {
                return false;
            }

            GraphNode nextCellOptional = graph.getNode(row, nextCell);
            if (nextCellOptional == null || nextCellOptional.getData() != EMPTY_CELL) {
                return false;
            }
        }

        return true;
    }

    private boolean canChangeOrientation(GraphNode cell, Graph graph) {
        int row = cell.getRow();
        int col = cell.getCol();

        int numRows = graph.getNumRows();
        int numCols = graph.getNumCols();

        // Verificar si hay celdas adyacentes que sean muro o estén fuera de la matriz con respecto al punto de rotación
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                int adjRow = row + i;
                int adjCol = col + j;

                // Si la celda está en el punto de rotación, omitir la comprobación
                if (adjRow == row && adjCol == col) {
                    continue;
                }

                // Comprobar si la celda está dentro de la matriz
                if (adjRow < 0 || adjRow >= numRows || adjCol < 0 || adjCol >= numCols) {
                    return false;
                }

                // Obtener la celda adyacente
                GraphNode adjacentCell = graph.getNode(adjRow, adjCol);

                // Comprobar si la celda adyacente es un muro o no está vacía
                if (adjacentCell == null || adjacentCell.getData() == WALL_CELL || adjacentCell.getData() != EMPTY_CELL) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean isValidMaze(Graph graph, GraphNode startNode, GraphNode endNode) {
        // Verificar si el laberinto es válido

        List<GraphNode> nodes = graph.getAllNodes();
        int numRows = graph.getNumRows();
        int numCols = graph.getNumCols();

        if (startNode == null || endNode == null) {
            return false;
        }

        // Verificar si el nodo de inicio y el nodo objetivo son celdas vacías
        if (startNode.getData() != EMPTY_CELL || endNode.getData() != EMPTY_CELL) {
            return false;
        }
        // Verificar caracteres validos
        for (GraphNode node : nodes) {
            char data = node.getData();
            if (data != EMPTY_CELL && data != WALL_CELL) {
                return false;
            }
        }

        // Verificar que los nodos están dentro de la matriz y que esta es cuadrada o rectangular
        for (GraphNode node : nodes) {
            int row = node.getRow();
            int col = node.getCol();

            if (row >= numRows || col >= numCols) {
                return false;
            }
        }

        return true;
    }


}
