package labyrinth.services.solvers;

import labyrinth.model.Graph;

/**
 * @author jasim
 */
public interface MazeSolver {
    int findShortestPath(Graph graph);
}
