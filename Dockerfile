FROM openjdk:17-jdk-alpine
WORKDIR /app
COPY out/*.jar app.jar
COPY src/java/labyrinth/sample/ src/java/labyrinth/sample/
ENTRYPOINT ["java", "-jar", "app.jar"]
